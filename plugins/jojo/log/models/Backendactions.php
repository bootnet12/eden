<?php namespace Jojo\Log\Models;

use Mail;
use BackendAuth;
class Backendactions extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'jojo_log_backendactions';

	protected $fillable = ['id','backenduser','actiontable','action','actiondate','created_at','updated_at','deleted_at','entityid','ipadress',];

	public $attributeNames = [
		"id" => "Id",
		"backenduser" => "Utilisateur",
		"actiontable" => "Table concernée",
		"action" => "Action",
		"actiondate" => "Date",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
		"entityid" => "Identifiant de l'entité",
		"ipadress" => "Adresse IP",
	];

	public $belongsTo = [
		'backenduser'=>[\Backend\Models\User::class],
	];

	public $hasMany = [
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}