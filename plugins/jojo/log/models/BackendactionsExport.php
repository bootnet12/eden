<?php namespace Jojo\Log\Models;

class BackendactionsExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Jojo\Log\Models\Backendactions::all();
        
            $records->each(function($record) use ($columns) {
                
			 if ($record->backenduser) $record->r_backenduser = $record->backenduser->first_name;

    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}