<?php namespace Jojo\Log;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
	
	 public function registerComponents()
    {
        return [
        
			'Jojo\Log\Components\FrontendCrudBackendactions'=>'FrontendCrudBackendactions',
			'Jojo\Log\Components\ListeBackendactions'=>'ListeBackendactions',

        ];
    }
    
    public function registerSettings()
    {
    }
    
}