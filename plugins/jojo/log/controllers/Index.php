<?php namespace Jojo\Log\Controllers;

class Index extends \Backend\Classes\Controller{

    public $requiredPermissions = ['jojo.log'];
    public function __construct(){
        parent::__construct();
        \BackendMenu::setContext('Jojo.Log','log');
    }

    public function index(){}

}