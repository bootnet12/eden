<?php namespace  Jojo\Log\Components;
use Cms\Classes\Page;
class ListeBackendactions extends \Cms\Classes\ComponentBase
{    
    public function componentDetails(){
        return [
            'name' => 'ListeBackendactions',
            'description' => 'ListeBackendactions'
        ];
    }
	
	public function defineProperties() {
        return [
		
		'itemsPerPage' => [
				'title'             => 'Nombre d\'items par page',
				'type'              => 'string',
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'Entrer un entier',
				'default'           => 12,
			],
		'viewPage' => [
                'title'             => 'View page',
                'type'              => 'dropdown',
                'group'             => 'Links',
                'options'           => $this->getPageOptions()
            ],
		'updatePage' => [
                'title'             => 'Update page',
                'type'              => 'dropdown',
                'group'             => 'Links',
                'options'           => $this->getPageOptions()
            ],
            
            
        ];
    }

	public function onRun(){ 
		$this->page['records']= \Jojo\Log\Models\Backendactions::paginate($this->property('itemsPerPage'),$this->param('page'));;
		$this->page['viewpage']=$this->property('viewPage');
		$this->page['updatepage']=$this->property('updatePage');
    }
	
	public function getPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }
	
	
	
} 