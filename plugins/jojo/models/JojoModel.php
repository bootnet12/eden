<?php namespace Jojo\Models;

use Model;
use BackendAuth;
use Auth;


class JojoModel extends Model
{
  //public $timestamps = false;
    public $table;
    /*
    public function beforeCreate()
    {
		if(BackendAuth::check()) {
			$user = BackendAuth::getUser();
			$this->firstuser=$user->id;
			return 0;
		}

		$user = Auth::getUser();
		$this->firstuser=$user->id;
    }
	
    public function beforeSave() {
		if(BackendAuth::check())
		{
			$user = BackendAuth::getUser();
			$this->lastuser=$user->id;
			return 0;
		}
		$user = Auth::getUser();
		if($user){
			$this->lastuser=$user->id;
		}
    }
	
    public function beforeDelete()
    {
		if(BackendAuth::check()) {
			$user = BackendAuth::getUser();
			$this->lastuser=$user->id;
			return 0;
		}
		$user = Auth::getUser();
		$this->lastuser=$user->id;
    }

*/
 /* public function afterUpdate() {
   // enregistrement dans la table de logs   
    if(BackendAuth::check()) {
      $user = BackendAuth::getUser();
      $log= new \Arre\Log\Models\Backendactions;
      $log->action='Mise à jour';
      $log->actiontable=$this->table;
      $log->actiondate=$this->updated_at;
      $log->entityid=$this->id;
      $log->backenduser_id=$user->id;
      $log->ipadress = \Request::getClientIp();
      $log->save();
      }
  }

  public function afterCreate() {
   // enregistrement dans la table de mlogs   
  if(BackendAuth::check()) {
      $user = BackendAuth::getUser();
      $log= new \Arre\Log\Models\Backendactions;
      $log->action='Nouvelle création';
      $log->actiontable=$this->table;
      $log->actiondate=$this->updated_at;
      $log->entityid=$this->id;
      $log->backenduser_id=$user->id;
      $log->ipadress = \Request::getClientIp();
      $log->save();
    }
  }
*/
    // calcul de temps entre deux dates
  public static function dureeEntreDate($dateDebut,$dateFin) {
    if($dateDebut && $dateFin)  {
	    $debut = new \DateTime($dateDebut);
	    $fin= new \DateTime($dateFin);
	    $interval = $fin->diff($debut);
	    $temps='';
	    if($interval->y > 0){$temps.=$interval->y.' an(s) ';}
	    if($interval->m > 0){$temps.=$interval->m.' mois ';}
	    if($interval->d > 0){$temps.=$interval->d.' jour(s)';}
      return $temps; 
    }
    else {return 'Indéfinie';} 
  }


    //fonction de conversion d'un nombre en lettre

public static function asLetters($number) {

    if ($number == "2001") return 'deux mille un';
    if ($number == "1901") return 'mille neuf cent un';
    if ($number == "01") return 'premier';
    if ($number == "02") return 'deux';
    if ($number == "03") return 'trois';
    if ($number == "04") return 'quatre';
    if ($number == "05") return 'cinq';
    if ($number == "06") return 'six';
    if ($number == "07") return 'sept';
    if ($number == "08") return 'huit';
    if ($number == "09") return 'neuf';
  
    $convert = explode('.', $number);
    $num[17] = array('zero', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit',
                     'neuf', 'dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize');
                      
    $num[100] = array(20 => 'vingt', 30 => 'trente', 40 => 'quarante', 50 => 'cinquante',
                      60 => 'soixante', 70 => 'soixante-dix', 80 => 'quatre-vingt', 90 => 'quatre-vingt-dix');
                                      
    if (isset($convert[1]) && $convert[1] != '') {
      return self::asLetters($convert[0]).' et '.self::asLetters($convert[1]);
    }
    if ($number < 0) return 'moins '.self::asLetters(-$number);
    if ($number < 17) {
      return $num[17][$number];
    }
    elseif ($number < 20) {
      return 'dix-'.self::asLetters($number-10);
    }
    elseif ($number < 100) {
      if ($number%10 == 0) {
        return $num[100][$number];
      }
      elseif (substr($number, -1) == 1) {
        if( ((int)($number/10)*10)<70 ){
          return self::asLetters((int)($number/10)*10).'-et-un';
        }
        elseif ($number == 71) {
          return 'soixante-et-onze';
        }
        elseif ($number == 81) {
          return 'quatre-vingt-un';
        }
        elseif ($number == 91) {
          return 'quatre-vingt-onze';
        }
      }
      elseif ($number < 70) {
        return self::asLetters($number-$number%10).'-'.self::asLetters($number%10);
      }
      elseif ($number < 80) {
        return self::asLetters(60).'-'.self::asLetters($number%20);
      }
      else {
        return self::asLetters(80).'-'.self::asLetters($number%20);
      }
    }
    elseif ($number == 100) {
      return 'cent';
    }
    elseif ($number < 200) {
      return self::asLetters(100).' '.self::asLetters($number%100);
    }
    elseif ($number < 1000) {
      return self::asLetters((int)($number/100)).' '.self::asLetters(100).($number%100 > 0 ? ' '.self::asLetters($number%100): '');
    }
    elseif ($number == 1000){
      return 'mille';
    }
    elseif ($number < 2000) {
      return self::asLetters(1000).' '.self::asLetters($number%1000).' ';
    }
    elseif ($number < 1000000) {
      return self::asLetters((int)($number/1000)).' '.self::asLetters(1000).($number%1000 > 0 ? ' '.self::asLetters($number%1000): '');
    }
    elseif ($number == 1000000) {
      return 'millions';
    }
    elseif ($number < 2000000) {
      return self::asLetters(1000000).' '.self::asLetters($number%1000000);
    }
    elseif ($number < 1000000000) {
      return self::asLetters((int)($number/1000000)).' '.self::asLetters(1000000).($number%1000000 > 0 ? ' '.self::asLetters($number%1000000): '');
    }
  }

public static function moisEnLettres($number){
       if ($number =="01") return 'janvier';
       if ($number =="02") return 'février';
       if ($number =="03") return 'mars';
       if ($number =="04") return 'avril';
       if ($number =="05") return 'mai';
       if ($number =="06") return 'juin';
       if ($number =="07") return 'juillet';
       if ($number =="08") return 'août';
       if ($number =="09") return 'septembre';
       if ($number =="10") return 'octobre';
       if ($number =="11") return 'novembre';
       if ($number =="12") return 'décembre';    
 }


/*
  public function getCreatedAtAttribute() {

      if(in_array('created_at', $this->fillable)){
        return $this->created_at; 
      }

      if(in_array('creation_date', $this->fillable)){
        return $this->creation_date;
      }

  }
  
  public function setCreatedAtAttribute($value) {

    if(in_array('created_at', $this->fillable)){
      $this->attributes['created_at'] = $value;
    }

    if(in_array('creation_date', $this->fillable)){
      $this->attributes['creation_date'] = $value;
    }

  }


   public function setUpdated_at($value) {

    if(in_array('update_at', $this->fillable)){
      $this->attributes['update_at'] = $value;
    }

    if(in_array('modification_date', $this->fillable)){
      $this->attributes['modification_date'] = $value;
    }

  }

  public function getUpdate_at() {

      if(in_array('updated_at', $this->fillable)){
        return $this->updated_at;
      }

      if(in_array('modification_date', $this->fillable)){
        return $this->modification_date;
      }

  }
*/

}
