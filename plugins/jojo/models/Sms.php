<?php namespace Digit\Models;

use Model;
use BackendAuth;
use Auth;
use Digit\Reporting\Models\NotificationsmsModel;

class Sms  
{
	//protected static $base_url="http://178.33.227.9:44319/paymentgatewaysimulator/sendsms";
    
	// pour l'envoi des sms
	
	public static function send($phoneNumber,$message){
        $url_service="http://178.33.227.9:44319/paymentgatewaysimulator/sendsms?phoneNumber=".'225'.$phoneNumber."&message=".urlencode($message);
        $data=null;
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url_service);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $reponse = curl_exec ($ch);
            curl_close ($ch);
        } catch (Exception $ex) {
            $ex->getMessage();
        }
        
        $notificationSms = new NotificationsmsModel();
        
        if(BackendAuth::check())
		{
			$user = BackendAuth::getUser();
			$notificationSms->first_user=$user->id;
		}
        elseif(Auth::check())
		{
			$user = Auth::getUser();
			$notificationSms->first_user=$user->id;
		}

        $resultatenvoi= json_decode($reponse);
        
        $notificationSms->objet="";
        $notificationSms->emetteur="";
        $notificationSms->destinataire = $phoneNumber;
        $notificationSms->message = $message;
        $notificationSms->erreurenvoi = $resultatenvoi->response;
        $notificationSms->statutmessage = $resultatenvoi->status;
        
        // historisation des sms
        $notificationSms->save();
        
        return $resultatenvoi->response;
    }

	
    
}
