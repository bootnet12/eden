<?php namespace Eden\Gestion;

use System\Classes\PluginBase;
use Eden\Gestion\Models\Achatproduit;
use RainLab\User\Models\User;
use DB;
class Plugin extends PluginBase
{
	
	 public function registerComponents()
    {
        return [
        
			'Eden\Gestion\Components\FrontendCrudAchatproduits'=>'FrontendCrudAchatproduits',
			'Eden\Gestion\Components\ListeAchatproduits'=>'ListeAchatproduits',
			'Eden\Gestion\Components\FrontendCrudComptebancaires'=>'FrontendCrudComptebancaires',
			'Eden\Gestion\Components\ListeComptebancaires'=>'ListeComptebancaires',
			'Eden\Gestion\Components\FrontendCrudComptemobiles'=>'FrontendCrudComptemobiles',
			'Eden\Gestion\Components\ListeComptemobiles'=>'ListeComptemobiles',
			'Eden\Gestion\Components\FrontendCrudProduits'=>'FrontendCrudProduits',
			'Eden\Gestion\Components\ListeProduits'=>'ListeProduits',
			'Eden\Gestion\Components\DetailProduit'=>'DetailProduit',
			'Eden\Gestion\Components\ListeActualites'=>'ListeActualites',
			'Eden\Gestion\Components\DetailActualite'=>'DetailActualite',
			'Eden\Gestion\Components\ListeCommandesProduits'=>'ListeCommandesProduits',
			'Eden\Gestion\Components\Home'=>'Home',
			'Eden\Gestion\Components\Filleule'=>'Filleule',
        ];
    }
    
    public function registerSettings()
    {
	}
	

	public function registerSchedule($schedule)
    {
		// Recuperation des achats validés n'ayant pas de bonus reversé
		/*$achats = Achatproduit::whereNull('reversement_bonus')->where('statut', 1)->get();
		foreach($achats as $achat){
			// trace_log($achat->id);
		}*/
		// Mise à jour des filleules
		$this->updateNbreFilleules();

		$schedule->call(function () {
			trace_log("Début de la mise à jour du nombre de filleules pour chaque utilisateur ...");
			$this->updateNbreFilleules();
			trace_log("Fin de la mise à jour du nombre de filleules pour chaque utilisateur ...");
        })->everyMinute();
	}

	// Mise à jour du nombre de filleules pour chaque utilisateur
	public function updateNbreFilleules(){
		DB::beginTransaction(); // Début de la transaction
		try{
			// Recuperation des utilisateurs dont la mise à jour n'a pas été effectué
			$users = User::whereNull('counted_at_nbre_filleules')->get();
			foreach($users as $user){
				// Mise à jour du filleule pour chaque génération
				$children = $user; // L'utilisateur devient un enfant
				$exit = true; // initialisation de l'enfant
				while($exit){
					$exit = false; // On force la sortie du programme
					// Recuperation du parrain
					$parrain = User::find($children->parrain_id);
					if($parrain){
						$parrain->nbre_filleules += 1; 
						$parrain->save();

						// On vérifie si le parrain a son tour à un parrain
						if($parrain->parrain_id){
							$children = $parrain;
							$exit = true;
						}
					}
				}
				$user->counted_at_nbre_filleules = now();
				$user->save();
			}
			DB::commit(); // Commit des opérations
		}catch(\Except $e){
			trca_log("Une erreur est survenue lors de la génération d'arbre pour un utilisateur , message: ".$e->getMessage());
			DB::rollback(); // Rollback des opération
		}
	}
    
}