<?php namespace Eden\Gestion\Apis;
    
use Cms\Classes\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;
use RainLab\User\Models\User as UserModel;

class Users extends Controller
{
    public function index(Request $request, $code_parrinage){
        $data = UserModel::where('code_parrainage', $code_parrinage)->first();
        return [
            "code" => 200,
            "data" => $data
        ];
    }

}