<?php namespace Eden\Gestion\Apis;
    
use Cms\Classes\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;

class Comptemobiles extends Controller
{
    public function index(){

        $data = \Eden\Gestion\Models\Comptemobile::all()->toArray();

         return [
                "code" => 200,
                "data" => $data
            ];
    }

    public function show($id){

        $data = \Eden\Gestion\Models\Comptemobile::where('id',$id)->get();

        if($data){

           return [
                "code" => 200,
                "data" => $data
            ];
        }
        return [
                   "code" => 400,
                   "message" => "bad request"
               ];
    }
    
    public function search($key,$value){

        $data = \Eden\Gestion\Models\Comptemobile::where($key,$value)->get();

        if($data){

           return [
                "code" => 200,
                "data" => $data
            ];
        }
        return [
                   "code" => 400,
                   "message" => "bad request"
               ];
    }

    public function store(Request $request){

    	try {
            $arr = $request->all();
            $instance = new \Eden\Gestion\Models\Comptemobile() ;    

            while ( $data = current($arr)) {
                $instance->{key($arr)} = $data;
                next($arr);
            }
            $instance->save();
            return [
                    "code" => 200,
                    "data" => $instance
                   ];
        }
        catch (Exception $ex) {
            $ex->getMessage();
            return $ex;
        }
    }

    public function update($id, Request $request){

        $status = \Eden\Gestion\Models\Comptemobile::where('id',$id)->update($request->all());
    
        if($status){
            return [
                    "code" => 200,
                    "data" => "Data has been updated successfully."
                   ];
        }else{
            return [
                    "code" => 400,
                    "data" => "Error, data failed to update."
                   ];
        }
    }

    public function delete($id){

        \Eden\Gestion\Models\Comptemobile::where('id',$id)->delete();
        return [
                   "code" => 200,
                   "data" => "Data has been deleted successfully."
                ];
    }

     
}