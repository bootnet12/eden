<?php namespace  Eden\Gestion\Components;

use Cms\Classes\Page;
use DB;

class Home extends \Cms\Classes\ComponentBase
{    
    public function componentDetails(){
        return [
            'name' => 'Home',
            'description' => 'Home'
        ];
    }

	public function onRun(){ 

        // recuperation de l'adresse ip
        $ip = $_SERVER['REMOTE_ADDR'];
        $countIp = DB::table('visites')->where('ip', $ip)->count();
        if($countIp == 0){
            DB::table('visites')->insert([
                'ip' => $ip,
                'timestamp' => time()
            ]);
            $countIp += 1;
        }
        $this->page['visiteurs'] = $countIp;
        $this->page['inscrits'] = DB::table('users')->count();

		$this->page['produits']= \Eden\Gestion\Models\Produit::take(3)->get();
        $this->page['actualites']= \Eden\Gestion\Models\Actualite::take(4)->get();
        $this->page['bannieres'] = \Eden\Configuration\Models\Banniere::all();
        $this->page['temoignages'] = \Eden\Configuration\Models\Temoignage::all();
    }
	
} 