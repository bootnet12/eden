<?php namespace  Eden\Gestion\Components;

use Cms\Classes\Page;
use Eden\Gestion\Models\AchatProduit;
use Auth;

class DetailActualite extends \Cms\Classes\ComponentBase
{    
    public function componentDetails(){
        return [
            'name' => 'DetailActualite',
            'description' => 'DetailActualite'
        ];
    }

	public function onRun(){ 
        $idActualite = $this->param('id_actualite');
        $this->page['record'] = \Eden\Gestion\Models\Actualite::find($idActualite);
    }
	
} 