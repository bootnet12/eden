<?php namespace  Eden\Gestion\Components;

use Cms\Classes\Page;
use RainLab\User\Models\User;
use Auth;

class ListeCommandesProduits extends \Cms\Classes\ComponentBase
{    
    public function componentDetails(){
        return [
            'name' => 'ListeCommandesProduits',
            'description' => 'ListeCommandesProduits'
        ];
    }
	
	public function defineProperties() {
        return [
		
		'itemsPerPage' => [
				'title'             => 'Nombre d\'items par page',
				'type'              => 'string',
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'Entrer un entier',
				'default'           => 7,
			],
		'viewPage' => [
                'title'             => 'View page',
                'type'              => 'dropdown',
                'group'             => 'Links',
                'options'           => $this->getPageOptions()
            ],
		'updatePage' => [
                'title'             => 'Update page',
                'type'              => 'dropdown',
                'group'             => 'Links',
                'options'           => $this->getPageOptions()
            ],
            
            
        ];
    }

	public function onRun(){ 
        $adeherantId = Auth::getUser()->id;
        $this->page['records'] = \Eden\Gestion\Models\Achatproduit::where('adherent_id', $adeherantId)
        ->orderBy('created_at', 'desc')->paginate($this->property('itemsPerPage'),$this->param('page'));;
		$this->page['viewpage']=$this->property('viewPage');
		$this->page['updatepage']=$this->property('updatePage');
    }
	
	public function getPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }
	
	
	
} 