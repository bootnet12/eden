<?php namespace  Eden\Gestion\Components;

use Cms\Classes\Page;
use Eden\Gestion\Models\Achatproduit;
use Auth;
use Flash;
use Lang;

class DetailProduit extends \Cms\Classes\ComponentBase
{    
    public function componentDetails(){
        return [
            'name' => 'DetailProduit',
            'description' => 'DetailProduit'
        ];
    }

	public function onRun(){
        $idProduit = $this->param('id_produit');
        $user = $this->page['user'] = Auth::getUser();
        $this->page['reference'] = get('reference');
		$this->page['record']= \Eden\Gestion\Models\Produit::find($idProduit);
        $this->page['errorMessageNoValidate'] = Lang::get('eden.gestion::lang.app.message_user_no_validate');
    }

    public function onPayConfirmProduit(){
        $idProduit = $this->param('id_produit');
        // Enregistrement d'un produit
        $achatProduit = new AchatProduit;
        $achatProduit->produit_id = post('idProduit');
        $achatProduit->adherent_id = Auth::getUser()->id;
        $achatProduit->reference = post('reference');
        $achatProduit->statut = 1;
        $achatProduit->save();
        return \Redirect::to("/pay-produit-success/".$idProduit);
    }

    public function onRequestProduit(){
        $idProduit = $this->param('id_produit');
        if(empty(post('reference'))){
            Flash::error("Veuillez entrez une référence !");
            return;
        }
        return \Redirect::to("pay-produit-confirmation/".$idProduit."?reference=".post('reference'));
    }

    // Reversement des bonus 
    public function reversementBonus(){
        // Reversement des bonus
    }
	
} 