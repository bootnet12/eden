<?php namespace  Eden\Gestion\Components;
use Cms\Classes\Page;
class PayProduitRequest extends \Cms\Classes\ComponentBase
{    
    public function componentDetails(){
        return [
            'name' => 'PayProduitRequest',
            'description' => 'PayProduitRequest'
        ];
    }

	public function onRun(){ 
		$idProduit = $this->param('id_produit');
		$this->page['record']= \Eden\Gestion\Models\Produit::find($idProduit);
    }	
} 