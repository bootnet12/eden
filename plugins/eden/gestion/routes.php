<?php 

Route::GET('api/eden/gestion/achatproduits/all', ['as' => 'api/eden/gestion/achatproduits/all', 'uses' => 'Eden\Gestion\Apis\Achatproduits@index']);
Route::GET('api/eden/gestion/achatproduits/find/{id}', ['as' => 'api/eden/gestion/achatproduits/find/{id}', 'uses' => 'Eden\Gestion\Apis\Achatproduits@show']);
Route::GET('api/eden/gestion/achatproduits/search/{key}/{value}', ['as' => 'api/eden/gestion/achatproduits/search/{key}/{value}', 'uses' => 'Eden\Gestion\Apis\Achatproduits@search']);
Route::POST('api/eden/gestion/achatproduits', ['as' => 'api/eden/gestion/achatproduits', 'uses' => 'Eden\Gestion\Apis\Achatproduits@store']);
Route::PUT('api/eden/gestion/achatproduits/{id}', ['as' => 'api/eden/gestion/achatproduits', 'uses' => 'Eden\Gestion\Apis\Achatproduits@update']);
Route::DELETE('api/eden/gestion/achatproduits/{id}', ['as' => 'api/eden/gestion/achatproduits', 'uses' => 'Eden\Gestion\Apis\Achatproduits@delete']);

Route::GET('api/eden/gestion/comptebancaires/all', ['as' => 'api/eden/gestion/comptebancaires/all', 'uses' => 'Eden\Gestion\Apis\Comptebancaires@index']);
Route::GET('api/eden/gestion/comptebancaires/find/{id}', ['as' => 'api/eden/gestion/comptebancaires/find/{id}', 'uses' => 'Eden\Gestion\Apis\Comptebancaires@show']);
Route::GET('api/eden/gestion/comptebancaires/search/{key}/{value}', ['as' => 'api/eden/gestion/comptebancaires/search/{key}/{value}', 'uses' => 'Eden\Gestion\Apis\Comptebancaires@search']);
Route::POST('api/eden/gestion/comptebancaires', ['as' => 'api/eden/gestion/comptebancaires', 'uses' => 'Eden\Gestion\Apis\Comptebancaires@store']);
Route::PUT('api/eden/gestion/comptebancaires/{id}', ['as' => 'api/eden/gestion/comptebancaires', 'uses' => 'Eden\Gestion\Apis\Comptebancaires@update']);
Route::DELETE('api/eden/gestion/comptebancaires/{id}', ['as' => 'api/eden/gestion/comptebancaires', 'uses' => 'Eden\Gestion\Apis\Comptebancaires@delete']);

Route::GET('api/eden/gestion/comptemobiles/all', ['as' => 'api/eden/gestion/comptemobiles/all', 'uses' => 'Eden\Gestion\Apis\Comptemobiles@index']);
Route::GET('api/eden/gestion/comptemobiles/find/{id}', ['as' => 'api/eden/gestion/comptemobiles/find/{id}', 'uses' => 'Eden\Gestion\Apis\Comptemobiles@show']);
Route::GET('api/eden/gestion/comptemobiles/search/{key}/{value}', ['as' => 'api/eden/gestion/comptemobiles/search/{key}/{value}', 'uses' => 'Eden\Gestion\Apis\Comptemobiles@search']);
Route::POST('api/eden/gestion/comptemobiles', ['as' => 'api/eden/gestion/comptemobiles', 'uses' => 'Eden\Gestion\Apis\Comptemobiles@store']);
Route::PUT('api/eden/gestion/comptemobiles/{id}', ['as' => 'api/eden/gestion/comptemobiles', 'uses' => 'Eden\Gestion\Apis\Comptemobiles@update']);
Route::DELETE('api/eden/gestion/comptemobiles/{id}', ['as' => 'api/eden/gestion/comptemobiles', 'uses' => 'Eden\Gestion\Apis\Comptemobiles@delete']);

Route::GET('api/eden/gestion/produits/all', ['as' => 'api/eden/gestion/produits/all', 'uses' => 'Eden\Gestion\Apis\Produits@index']);
Route::GET('api/eden/gestion/produits/find/{id}', ['as' => 'api/eden/gestion/produits/find/{id}', 'uses' => 'Eden\Gestion\Apis\Produits@show']);
Route::GET('api/eden/gestion/produits/search/{key}/{value}', ['as' => 'api/eden/gestion/produits/search/{key}/{value}', 'uses' => 'Eden\Gestion\Apis\Produits@search']);
Route::POST('api/eden/gestion/produits', ['as' => 'api/eden/gestion/produits', 'uses' => 'Eden\Gestion\Apis\Produits@store']);
Route::PUT('api/eden/gestion/produits/{id}', ['as' => 'api/eden/gestion/produits', 'uses' => 'Eden\Gestion\Apis\Produits@update']);
Route::DELETE('api/eden/gestion/produits/{id}', ['as' => 'api/eden/gestion/produits', 'uses' => 'Eden\Gestion\Apis\Produits@delete']);

Route::GET('api/eden/users/{code_parrainage}', '\Eden\Gestion\Apis\Users@index');

