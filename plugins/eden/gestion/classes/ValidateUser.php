<?php namespace Eden\Gestion\Classes;

use Backend\Classes\Controller;
use BackendMenu;
use RainLab\User\Models\User;
use DB;
use Flash;
use Eden\Transaction\Models\Reversementbonus;
use Eden\Gestion\Classes\Exceptions\ValidateException;

class ValidateUser
{

    public function validated($id){
        DB::beginTransaction(); // Début de la transaction
        try{
        	// Recuperation de l'utilisateur
        	$user = User::find($id);
        	if(!$user->validated_souscription_admin_at){
        		$user->validated_souscription_admin_at = now();
        		$user->save();

                // Reversement bonus
                $this->reversementBonus($id);
            }else{
                throw new ValidateException("Désolé ce compte a déja été validé !");
            }
            DB::commit(); // Commit Transaction
            return true;
        }catch(\Exception $e){
            trace_log("Une erreur est survenue lors de validation d'une demande de paiement , message:".$e->getMessage());
            DB::rollback();
            throw new Exception("Une erreur est survenue lors du traitement !");
        }
    }

    // Reversement de bonus
    public function reversementBonus($adherantId){
        // Reversement de bonus de la personne
        $personne = User::find($adherantId);
        
        
        // Reversement de bonus du partenaire direct
        $parrainDirect = User::find($personne->parrain_id);
        if($parrainDirect){
            $bonus= \Eden\Configuration\Models\Parametre::where('cle','bonusparrainnagedirect')->first();
            $parrainDirect->solde += $bonus->valeur;
            $parrainDirect->save();
            $reversementParrainDirect = new Reversementbonus();
            $reversementParrainDirect->montant = $bonus->valeur;
            $reversementParrainDirect->adherent_id = $parrainDirect->id;
            $reversementParrainDirect->souscriptionadherant_id = $adherantId;
            $reversementParrainDirect->save();

            // Reversement de bonus au partenaire de 2ème génération
            $parrainIndirect = User::find($parrainDirect->parrain_id);
            if($parrainIndirect){
                $bonus= \Eden\Configuration\Models\Parametre::where('cle','bonusparrainnage2eme')->first();
                $parrainIndirect->solde += $bonus->valeur;
                $parrainIndirect->save();
                $reversementParrainInDirect = new Reversementbonus();
                $reversementParrainInDirect->montant = $bonus->valeur;
                $reversementParrainInDirect->adherent_id = $parrainIndirect->id;
                $reversementParrainInDirect->souscriptionadherant_id = $adherantId;
                $reversementParrainInDirect->save();
            }
        }
    }

}