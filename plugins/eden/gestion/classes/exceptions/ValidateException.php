<?php namespace Eden\Gestion\Classes\Exceptions;

class ValidateException extends \Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message, Throwable $previous = null) {
        // some code

        // make sure everything is assigned properly
        parent::__construct($message, 0, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function customFunction() {
        echo "Une erreur est survenue lors de la validation de l'utilisateur\n";
    }
}