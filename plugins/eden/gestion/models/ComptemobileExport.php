<?php namespace Eden\Gestion\Models;

class ComptemobileExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Eden\Gestion\Models\Comptemobile::all();
        
            $records->each(function($record) use ($columns) {
                
			 if ($record->pays) $record->r_pays = $record->pays->code;

			 if ($record->adherent) $record->r_adherent = $record->adherent->name;

    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}