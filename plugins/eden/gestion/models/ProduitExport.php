<?php namespace Eden\Gestion\Models;

class ProduitExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Eden\Gestion\Models\Produit::all();
        
            $records->each(function($record) use ($columns) {
                
			 if ($record->type) $record->r_type = $record->type->libelle;

			 if ($record->devisebonus) $record->r_devisebonus = $record->devisebonus->symbole;

    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}