<?php namespace Eden\Gestion\Models;

use Mail;
use BackendAuth;
class Actualite extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_gestion_actualites';

	protected $fillable = ['id','libelle','decription', 'published_at', 'firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"libelle" => "Libellé",
		"description" => "Description",
		"published_at" => "Published_at",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $attachOne = [
		'cover' =>  \System\Models\File::class,
	];

	public $hasMany = [
	];

	public $belongsToMany = [
	];

	public $attachMany = [
	];

}