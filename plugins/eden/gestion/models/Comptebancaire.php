<?php namespace Eden\Gestion\Models;

use Mail;
use BackendAuth;
class Comptebancaire extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_gestion_comptebancaire';

	protected $fillable = ['id','pays','banque','rib','adherent','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"pays" => "Pays",
		"banque" => "Banque",
		"rib" => "Rib",
		"adherent" => "Adherent",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
		'pays'=>[\Eden\Configuration\Models\Pays::class],
		'adherent'=>[\RainLab\User\Models\User::class],
	];

	public $hasMany = [
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}