<?php namespace Eden\Gestion\Models;

use Mail;
use BackendAuth;
class Produit extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_gestion_produit';

	protected $fillable = ['id','nom','prix','type','description','bonus_personnel','bonus_direct','bonus_second','devisebonus','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"nom" => "Nom",
		"prix" => "Prix",
		"type" => "Type",
		"description" => "Description",
		"bonus_personnel" => "Bonus_personnel",
		"bonus_direct" => "Bonus_direct",
		"bonus_second" => "Bonus_second",
		"devisebonus" => "Devisebonus",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
		'type'=>[\Eden\Configuration\Models\Typeproduit::class],
		'devisebonus'=>[\Eden\Configuration\Models\Devisebonus::class],
	];

	public $hasMany = [
        'achatproduits' => [
            'Eden\Gestion\Models\Achatproduit',
            'key' => 'produit_id',
        ],
	];

	public $belongsToMany = [
	];

	public $attachOne = [
		'cover' =>  \System\Models\File::class,
	];

	public $attachMany = [
		'images' =>  \System\Models\File::class,
	];

}