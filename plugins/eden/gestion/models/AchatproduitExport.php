<?php namespace Eden\Gestion\Models;

class AchatproduitExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Eden\Gestion\Models\Achatproduit::all();
        
            $records->each(function($record) use ($columns) {
                
			 if ($record->adherent) $record->r_adherent = $record->adherent->name;

			 if ($record->produit) $record->r_produit = $record->produit->nom;

    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}