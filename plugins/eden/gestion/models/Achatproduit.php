<?php namespace Eden\Gestion\Models;

use Mail;
use BackendAuth;
class Achatproduit extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_gestion_achatproduit';

	protected $fillable = ['id','adherent','produit','firstuser','lastuser','created_at','updated_at','deleted_at','statut'];

	public $attributeNames = [
		"id" => "Id",
		"adherent" => "Adherent",
		"produit" => "Produit",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
		'adherent'=>[\RainLab\User\Models\User::class],
		'produit'=>[\Eden\Gestion\Models\Produit::class],
		'status'=>[\Eden\Configuration\Models\Statustransaction::class, "key" => "statut", "otherKey" => "id"],
	];

	public $hasMany = [
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}