<?php namespace Eden\Gestion\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdenGestionComptemobile extends Migration
{
    public function up()
    {
        Schema::create('eden_gestion_comptemobile', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('numero', 60);
            $table->integer('adherent_id')->unsigned();
            $table->integer('firstuser')->nullable()->unsigned();
            $table->integer('lastuser')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('eden_gestion_comptemobile');
    }
}
