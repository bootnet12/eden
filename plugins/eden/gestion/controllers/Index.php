<?php namespace Eden\Gestion\Controllers;

class Index extends \Backend\Classes\Controller{

    public $requiredPermissions = ['eden.gestion'];
    public function __construct(){
        parent::__construct();
        \BackendMenu::setContext('Eden.Gestion','gestion');
    }

    public function index(){}

}