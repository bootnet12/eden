<?php namespace Eden\Gestion\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use RainLab\User\Models\User;
use DB;
use Flash;
use Eden\Transaction\Models\Reversementbonus;

class Adherents extends Controller
{
    public $implement = ['Backend\Behaviors\RelationController','Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Eden.Gestion', 'gestion', 'adherents');
    }

    public function onValidated(){
        DB::beginTransaction(); // Début de la transaction
        try{
            $id = post('checked')[0];
        	// Recuperation de l'utilisateur
        	$user = User::find($id);
        	if(!$user->validated_souscription_admin_at){
        		$user->validated_souscription_admin_at = now();
        		$user->save();

                // Reversement bonus
                $this->reversementBonus($id);
                Flash::success("Le compte adhérant a été validé avec succès !");
            }else{
            	Flash::error('Désolé ce compte a déja été validé !');
            }
            DB::commit(); // Commit Transaction
            return $this->listRefresh();
        }catch(\Exception $e){
            \Flash::error("Une erreur est survenue lors du traitement !");
            trace_log("Une erreur est survenue lors de validation d'une demande de paiement , message:".$e->getMessage());
            DB::rollback();
        }
    }

    // Reversement de bonus
    public function reversementBonus($adherantId){
        // Reversement de bonus de la personne
        $personne = User::find($adherantId);
        
        // Reversement de bonus du partenaire direct
        $parrainDirect = User::find($personne->parrain_id);
        if($parrainDirect){
            $parrainDirect->solde += 2500;
            $parrainDirect->save();
            $reversementParrainDirect = new Reversementbonus();
            $reversementParrainDirect->montant = 2500;
            $reversementParrainDirect->adherent_id = $parrainDirect->id;
            $reversementParrainDirect->souscriptionadherant_id = $adherantId;
            $reversementParrainDirect->save();

            // Reversement de bonus au partenaire de 2ème génération
            $parrainIndirect = User::find($parrainDirect->parrain_id);
            if($parrainIndirect){
                $parrainIndirect->solde += 500;
                $parrainIndirect->save();
                $reversementParrainInDirect = new Reversementbonus();
                $reversementParrainInDirect->montant = 500;
                $reversementParrainInDirect->adherent_id = $parrainIndirect->id;
                $reversementParrainInDirect->souscriptionadherant_id = $adherantId;
                $reversementParrainInDirect->save();
            }
        }
    }

}
