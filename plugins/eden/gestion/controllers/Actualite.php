<?php namespace Eden\Gestion\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Eden\Gestion\Models\Actualite as ActualiteModel;

class Actualite extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Eden.Gestion', 'gestion');
    }


    public function onPublised(){
        foreach(post('checked') as $id){
            // Recuperation de l'actualité
            $actualite = ActualiteModel::find($id);

            if($actualite->published_at){
                \Flash::error("Désolé cette actualité a déja été utilisé !");
            }
            $actualite->published_at = now();
            $actualite->save();
        }
        \Flash::success("L'actualité a été publié avec succès !");
        return $this->listRefresh();
    }
}
