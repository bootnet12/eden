<?php namespace Eden\Gestion\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use Redirect;
use Backend;
use Lang;
use Flash;
use ApplicationException;
use Mail;
use Backend\Classes\ControllerBehavior;
use Event;
use Str;
use Eden\Gestion\Models\Achatproduit;
use Eden\Transaction\Models\Reversementbonus;
use DB;
use RainLab\User\Models\User;

class Achatproduits extends Controller{

	public $implement = ['Jojo.Behaviors.ImportExportController','Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\RelationController'];

	public $listConfig = 'config_list.yaml';
	public $formConfig = 'config_form.yaml';
	public $relationConfig = 'config_relation.yaml';
	//public $reorderConfig = 'config_reorder.yaml';
	public $importExportConfig = 'config_import_export.yaml';
	//public $relationConfig = 'config_relation.yaml';

	public function __construct(){
		parent::__construct();
		BackendMenu::setContext('Eden.Gestion','gestion','achatproduits');
	}

	public function onCreateForm()
    {
        $this->asExtension('FormController')->create();
        return $this->makePartial('create_form');
    }

    public function onCreate()
    {
        $this->asExtension('FormController')->create_onSave();
        return $this->listRefresh();
    }

    public function onUpdateForm()
    {
        $this->asExtension('FormController')->update(post('record_id'));
        $this->vars['recordId'] = post('record_id');
        return $this->makePartial('update_form');
    }

    public function onUpdate()
    {
        $this->asExtension('FormController')->update_onSave(post('record_id'));
         return $this->listRefresh();
    }

    public function onDelete()
    {
        $this->asExtension('FormController')->update_onDelete(post('record_id'));
         return $this->listRefresh();
    }

    public function onValidated(){
        DB::beginTransaction(); // Début de la transaction
        try{
            foreach(post('checked') as $id){
                // Recuperation de la commande
                $commande = Achatproduit::find($id);

                if($commande->statut != 1){
                    \Flash::error("Désolé cette commande a été déja traité !");
                    return $this->listRefresh();
                }

                // Mise à jour de la demande
                $commande->statut = 2;
                // $retraitArgent->valideur_id = \BackendAuth::getUser() ? \BackendAuth::getUser()->id : null;
                $commande->validated_at = now();
                $commande->save();

                // Reversement bonus
                $this->reversementBonus($commande);

            }  
            DB::commit(); // Commit Transaction
            \Flash::success("Votre commande a été traité avec succès !");
            return $this->listRefresh();
        }catch(\Exception $e){
            \Flash::error("Une erreur est survenue lors du traitement !");
            trace_log("Une erreur est survenue lors de validation d'une demande de paiement , message:".$e->getMessage());
            DB::rollback();
        }
    }

    public function onCanceled(){
        foreach(post('checked') as $id){
            // Recuperation de la commande
            $commande = AchatProduit::find($id);

            if($commande->statut != 1){
                \Flash::error("Désolé cette commande a été déja traité !");
                return $this->listRefresh();
            }

            // Mise à jour de la commande
            $commande->statut = 3;
            $commande->save();
        }
        \Flash::success("Votre commande a été annulé avec succès !");
        return $this->listRefresh();
    }

    // Reversement de bonus
    public function reversementBonus($commande){
        // Reversement de bonus de la personne
        $personne = User::find($commande->adherent_id);
        $personne->solde += 500;
        $personne->save();
        $reversementPersonne = new Reversementbonus();
        $reversementPersonne->montant = 500;
        $reversementPersonne->adherent_id = $personne->id;
        $reversementPersonne->achatproduit_id = $commande->id;
        $reversementPersonne->save();
        
        // Reversement de bonus du partenaire direct
        $parrainDirect = User::find($personne->parrain_id);
        if($parrainDirect){
            $parrainDirect->solde += 2000;
            $parrainDirect->save();
            $reversementParrainDirect = new Reversementbonus();
            $reversementParrainDirect->montant = 2000;
            $reversementParrainDirect->adherent_id = $parrainDirect->id;
            $reversementParrainDirect->achatproduit_id = $commande->id;
            $reversementParrainDirect->save();

            // Reversement de bonus au partenaire de 2ème génération
            $parrainIndirect = User::find($parrainDirect->parrain_id);
            if($parrainIndirect){
                $parrainIndirect->solde += 500;
                $parrainIndirect->save();
                $reversementParrainInDirect = new Reversementbonus();
                $reversementParrainInDirect->montant = 500;
                $reversementParrainInDirect->adherent_id = $parrainIndirect->id;
                $reversementParrainInDirect->achatproduit_id = $commande->id;
                $reversementParrainInDirect->save();
            }
        }
    }

}