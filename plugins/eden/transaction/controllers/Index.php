<?php namespace Eden\Transaction\Controllers;

class Index extends \Backend\Classes\Controller{

    public $requiredPermissions = ['eden.transaction'];
    public function __construct(){
        parent::__construct();
        \BackendMenu::setContext('Eden.Transaction','transaction');
    }

    public function index(){}

}