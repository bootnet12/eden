<?php namespace Eden\Transaction\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use Redirect;
use Backend;
use Lang;
use Flash;
use ApplicationException;
use Mail;
use Backend\Classes\ControllerBehavior;
use Event;
use Str;

class Fraisinscriptions extends Controller{

	public $implement = ['Jojo.Behaviors.ImportExportController','Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\RelationController'];

	public $listConfig = 'config_list.yaml';
	public $formConfig = 'config_form.yaml';
	public $relationConfig = 'config_relation.yaml';
	//public $reorderConfig = 'config_reorder.yaml';
	public $importExportConfig = 'config_import_export.yaml';
	//public $relationConfig = 'config_relation.yaml';

	public function __construct(){
		parent::__construct();
		BackendMenu::setContext('Eden.Transaction','transaction','fraisinscriptions');
	}

	public function onCreateForm()
    {
        $this->asExtension('FormController')->create();
        return $this->makePartial('create_form');
    }

    public function onCreate()
    {
        $this->asExtension('FormController')->create_onSave();
        return $this->listRefresh();
    }

    public function onUpdateForm()
    {
        $this->asExtension('FormController')->update(post('record_id'));
        $this->vars['recordId'] = post('record_id');
        return $this->makePartial('update_form');
    }

    public function onUpdate()
    {
        $this->asExtension('FormController')->update_onSave(post('record_id'));
         return $this->listRefresh();
    }

    public function onDelete()
    {
        $this->asExtension('FormController')->update_onDelete(post('record_id'));
         return $this->listRefresh();
    }

}