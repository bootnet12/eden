<?php namespace Eden\Transaction\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use Redirect;
use Backend;
use Lang;
use Flash;
use ApplicationException;
use Mail;
use Backend\Classes\ControllerBehavior;
use Event;
use Str;
use Eden\Transaction\Models\Retraitargent;
use DB;

class Retraitargents extends Controller{

	public $implement = ['Jojo.Behaviors.ImportExportController','Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\RelationController'];

	public $listConfig = 'config_list.yaml';
	public $formConfig = 'config_form.yaml';
	public $relationConfig = 'config_relation.yaml';
	//public $reorderConfig = 'config_reorder.yaml';
	public $importExportConfig = 'config_import_export.yaml';
	//public $relationConfig = 'config_relation.yaml';

	public function __construct(){
		parent::__construct();
		BackendMenu::setContext('Eden.Transaction','transaction','retraitargents');
	}

	public function onCreateForm()
    {
        $this->asExtension('FormController')->create();
        return $this->makePartial('create_form');
    }

    public function onCreate()
    {
        $this->asExtension('FormController')->create_onSave();
        return $this->listRefresh();
    }

    public function onUpdateForm()
    {
        $this->asExtension('FormController')->update(post('record_id'));
        $this->vars['recordId'] = post('record_id');
        return $this->makePartial('update_form');
    }

    public function onUpdate()
    {
        $this->asExtension('FormController')->update_onSave(post('record_id'));
         return $this->listRefresh();
    }

    public function onDelete()
    {
        $this->asExtension('FormController')->update_onDelete(post('record_id'));
         return $this->listRefresh();
    }

    public function onValidated(){
        DB::beginTransaction();
        try{
            foreach(post('checked') as $id){
                // Recuperation de la demande 
                $retraitArgent = Retraitargent::find($id);

                if($retraitArgent->status_id != 1){
                    \Flash::error("Désolé cette demande de retrait ne peut être traitée !");
                    return $this->listRefresh();
                }

                // Mise à jour de la demande
                $retraitArgent->status_id = 2;
                $retraitArgent->valideur_id = \BackendAuth::getUser() ? \BackendAuth::getUser()->id : null;
                $retraitArgent->save();
    
                // Mise à jour du solde de l'utilisateur
                $user = $retraitArgent->adherent;
                $user->solde = $user->solde - $retraitArgent->montant;
                $user->save();
            }  
            // Commit Transaction
            DB::commit();
            \Flash::success("Votre demande de retrait a été validé avec succès !");
            return $this->listRefresh();
        }catch(\Exception $e){
            \Flash::error("Une erreur est survenue lors du traitement !");
            trace_log("Une erreur est survenue lors de validation d'une demande de paiement , message:".$e->getMessage());
            // Rollback Transaction
            DB::rollback();
        }
    }

    public function onCanceled(){
        foreach(post('checked') as $id){
            // Recuperation de la demande 
            $retraitArgent = Retraitargent::find($id);

            if($retraitArgent->status_id != 1){
                \Flash::error("Désolé cette demande de retrait ne peut être traitée !");
                return $this->listRefresh();
            }

            // Mise à jour de la demande de retrait
            $retraitArgent->status_id = 3;
            $retraitArgent->save();
        }
        \Flash::success("Votre demande de retrait a été annulé avec succès !");
        return $this->listRefresh();
    }

}