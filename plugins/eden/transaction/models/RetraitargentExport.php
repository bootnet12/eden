<?php namespace Eden\Transaction\Models;

class RetraitargentExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Eden\Transaction\Models\Retraitargent::all();
        
            $records->each(function($record) use ($columns) {
                
			 if ($record->adherent) $record->r_adherent = $record->adherent->name;

			 if ($record->valideur) $record->r_valideur = $record->valideur->first_name;

			 if ($record->status) $record->r_status = $record->status->libelle;

    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}