<?php namespace Eden\Transaction\Models;

class AchatunitesExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Eden\Transaction\Models\Achatunites::all();
        
            $records->each(function($record) use ($columns) {
                
			 if ($record->pays) $record->r_pays = $record->pays->code;

			 if ($record->operateur) $record->r_operateur = $record->operateur->nom;

			 if ($record->adherent) $record->r_adherent = $record->adherent->name;

			 if ($record->status) $record->r_status = $record->status->libelle;

    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}