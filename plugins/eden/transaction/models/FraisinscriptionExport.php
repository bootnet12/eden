<?php namespace Eden\Transaction\Models;

class FraisinscriptionExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Eden\Transaction\Models\Fraisinscription::all();
        
            $records->each(function($record) use ($columns) {
                
			 if ($record->adherent) $record->r_adherent = $record->adherent->name;

			 if ($record->status) $record->r_status = $record->status->libelle;

			 if ($record->valideur) $record->r_valideur = $record->valideur->first_name;

    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}