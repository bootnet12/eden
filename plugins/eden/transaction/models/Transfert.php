<?php namespace Eden\Transaction\Models;

use Mail;
use BackendAuth;
use Eden\Configuration\Models\Transaction;
class Transfert extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_transaction_transfert';

	protected $fillable = ['id','montant','expediteur','destinataire','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"montant" => "Montant",
		"expediteur" => "Expediteur",
		"destinataire" => "Destinataire",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
		'expediteur'=>[\RainLab\User\Models\User::class],
		'destinataire'=>[\RainLab\User\Models\User::class],
	];

	public $hasMany = [
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

	public function afterCreate(){
		$transaction = new Transaction;
		$transaction->montant = -1*$this->montant;
		$transaction->adherent_id = $this->expediteur_id;
		$transaction->save();

		$transaction = new Transaction;
		$transaction->montant = $this->montant;
		$transaction->adherent_id = $this->destinataire_id;
		$transaction->save();
	}

}