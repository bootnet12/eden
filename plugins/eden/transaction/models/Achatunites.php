<?php namespace Eden\Transaction\Models;

use Mail;
use BackendAuth;
class Achatunites extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_transaction_achatunites';

	protected $fillable = ['id','pays','operateur','telephone','montant','adherent','status','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"pays" => "Pays",
		"operateur" => "Operateur",
		"telephone" => "Telephone",
		"montant" => "Montant",
		"adherent" => "Adherent",
		"status" => "Status",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
		'pays'=>[\Eden\Configuration\Models\Pays::class],
		'operateur'=>[\Eden\Configuration\Models\Operateur::class],
		'adherent'=>[\RainLab\User\Models\User::class],
		'status'=>[\Eden\Configuration\Models\Statusachatunite::class],
	];

	public $hasMany = [
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}