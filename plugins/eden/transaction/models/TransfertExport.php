<?php namespace Eden\Transaction\Models;

class TransfertExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Eden\Transaction\Models\Transfert::all();
        
            $records->each(function($record) use ($columns) {
                
			 if ($record->expediteur) $record->r_expediteur = $record->expediteur->name;

			 if ($record->destinataire) $record->r_destinataire = $record->destinataire->name;

    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}