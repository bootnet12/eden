<?php namespace Eden\Transaction\Models;

use Mail;
use BackendAuth;
class Retraitargent extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_transaction_retraitargent';

	protected $fillable = ['id','montant','adherent','detailsoperation','valideur','status','confirmeparadherent','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"montant" => "Montant",
		"adherent" => "Adherent",
		"detailsoperation" => "Detailsoperation",
		"valideur" => "Valideur",
		"status" => "Status",
		"confirmeparadherent" => "Confirmeparadherent",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
		'adherent'=>[\RainLab\User\Models\User::class],
		'valideur'=>[\Backend\Models\User::class],
		'status'=>[\Eden\Configuration\Models\Statustransaction::class],
	];

	public $hasMany = [
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}