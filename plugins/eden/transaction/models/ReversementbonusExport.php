<?php namespace Eden\Transaction\Models;

class ReversementbonusExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Eden\Transaction\Models\Reversementbonus::all();
        
            $records->each(function($record) use ($columns) {
                
			 if ($record->adherent) $record->r_adherent = $record->adherent->name;

    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}