<?php namespace Eden\Transaction\Models;

use Mail;
use BackendAuth;
class Fraisinscription extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_transaction_fraisinscription';

	protected $fillable = ['id','datetransaction','montant','adherent','detailsoperation','status','valideur','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"datetransaction" => "Datetransaction",
		"montant" => "Montant",
		"adherent" => "Adherent",
		"detailsoperation" => "Detailsoperation",
		"status" => "Status",
		"valideur" => "Valideur",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
		'adherent'=>[\RainLab\User\Models\User::class],
		'status'=>[\Eden\Configuration\Models\Statustransaction::class],
		'valideur'=>[\Backend\Models\User::class],
	];

	public $hasMany = [
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}