<?php namespace Eden\Transaction\Models;

use Mail;
use BackendAuth;
use Eden\Configuration\Models\Transaction;

class Reversementbonus extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_transaction_reversementbonus';

	protected $fillable = ['id','montant','adherent','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"montant" => "Montant",
		"adherent" => "Adherent",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
		'adherent'=>[\RainLab\User\Models\User::class],
	];

	public function afterCreate(){
		$transaction = new Transaction;
		$transaction->montant = $this->montant;
		$transaction->adherent_id = $this->adherent_id;
		$transaction->save();
	}

	public $hasMany = [
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}