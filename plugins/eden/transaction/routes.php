<?php 

Route::GET('api/eden/transaction/achatunites/all', ['as' => 'api/eden/transaction/achatunites/all', 'uses' => 'Eden\Transaction\Apis\Achatunites@index']);
Route::GET('api/eden/transaction/achatunites/find/{id}', ['as' => 'api/eden/transaction/achatunites/find/{id}', 'uses' => 'Eden\Transaction\Apis\Achatunites@show']);
Route::GET('api/eden/transaction/achatunites/search/{key}/{value}', ['as' => 'api/eden/transaction/achatunites/search/{key}/{value}', 'uses' => 'Eden\Transaction\Apis\Achatunites@search']);
Route::POST('api/eden/transaction/achatunites', ['as' => 'api/eden/transaction/achatunites', 'uses' => 'Eden\Transaction\Apis\Achatunites@store']);
Route::PUT('api/eden/transaction/achatunites/{id}', ['as' => 'api/eden/transaction/achatunites', 'uses' => 'Eden\Transaction\Apis\Achatunites@update']);
Route::DELETE('api/eden/transaction/achatunites/{id}', ['as' => 'api/eden/transaction/achatunites', 'uses' => 'Eden\Transaction\Apis\Achatunites@delete']);

Route::GET('api/eden/transaction/fraisinscriptions/all', ['as' => 'api/eden/transaction/fraisinscriptions/all', 'uses' => 'Eden\Transaction\Apis\Fraisinscriptions@index']);
Route::GET('api/eden/transaction/fraisinscriptions/find/{id}', ['as' => 'api/eden/transaction/fraisinscriptions/find/{id}', 'uses' => 'Eden\Transaction\Apis\Fraisinscriptions@show']);
Route::GET('api/eden/transaction/fraisinscriptions/search/{key}/{value}', ['as' => 'api/eden/transaction/fraisinscriptions/search/{key}/{value}', 'uses' => 'Eden\Transaction\Apis\Fraisinscriptions@search']);
Route::POST('api/eden/transaction/fraisinscriptions', ['as' => 'api/eden/transaction/fraisinscriptions', 'uses' => 'Eden\Transaction\Apis\Fraisinscriptions@store']);
Route::PUT('api/eden/transaction/fraisinscriptions/{id}', ['as' => 'api/eden/transaction/fraisinscriptions', 'uses' => 'Eden\Transaction\Apis\Fraisinscriptions@update']);
Route::DELETE('api/eden/transaction/fraisinscriptions/{id}', ['as' => 'api/eden/transaction/fraisinscriptions', 'uses' => 'Eden\Transaction\Apis\Fraisinscriptions@delete']);

Route::GET('api/eden/transaction/rechargementcomptes/all', ['as' => 'api/eden/transaction/rechargementcomptes/all', 'uses' => 'Eden\Transaction\Apis\Rechargementcomptes@index']);
Route::GET('api/eden/transaction/rechargementcomptes/find/{id}', ['as' => 'api/eden/transaction/rechargementcomptes/find/{id}', 'uses' => 'Eden\Transaction\Apis\Rechargementcomptes@show']);
Route::GET('api/eden/transaction/rechargementcomptes/search/{key}/{value}', ['as' => 'api/eden/transaction/rechargementcomptes/search/{key}/{value}', 'uses' => 'Eden\Transaction\Apis\Rechargementcomptes@search']);
Route::POST('api/eden/transaction/rechargementcomptes', ['as' => 'api/eden/transaction/rechargementcomptes', 'uses' => 'Eden\Transaction\Apis\Rechargementcomptes@store']);
Route::PUT('api/eden/transaction/rechargementcomptes/{id}', ['as' => 'api/eden/transaction/rechargementcomptes', 'uses' => 'Eden\Transaction\Apis\Rechargementcomptes@update']);
Route::DELETE('api/eden/transaction/rechargementcomptes/{id}', ['as' => 'api/eden/transaction/rechargementcomptes', 'uses' => 'Eden\Transaction\Apis\Rechargementcomptes@delete']);

Route::GET('api/eden/transaction/retraitargents/all', ['as' => 'api/eden/transaction/retraitargents/all', 'uses' => 'Eden\Transaction\Apis\Retraitargents@index']);
Route::GET('api/eden/transaction/retraitargents/find/{id}', ['as' => 'api/eden/transaction/retraitargents/find/{id}', 'uses' => 'Eden\Transaction\Apis\Retraitargents@show']);
Route::GET('api/eden/transaction/retraitargents/search/{key}/{value}', ['as' => 'api/eden/transaction/retraitargents/search/{key}/{value}', 'uses' => 'Eden\Transaction\Apis\Retraitargents@search']);
Route::POST('api/eden/transaction/retraitargents', ['as' => 'api/eden/transaction/retraitargents', 'uses' => 'Eden\Transaction\Apis\Retraitargents@store']);
Route::PUT('api/eden/transaction/retraitargents/{id}', ['as' => 'api/eden/transaction/retraitargents', 'uses' => 'Eden\Transaction\Apis\Retraitargents@update']);
Route::DELETE('api/eden/transaction/retraitargents/{id}', ['as' => 'api/eden/transaction/retraitargents', 'uses' => 'Eden\Transaction\Apis\Retraitargents@delete']);

Route::GET('api/eden/transaction/reversementbonus/all', ['as' => 'api/eden/transaction/reversementbonus/all', 'uses' => 'Eden\Transaction\Apis\Reversementbonus@index']);
Route::GET('api/eden/transaction/reversementbonus/find/{id}', ['as' => 'api/eden/transaction/reversementbonus/find/{id}', 'uses' => 'Eden\Transaction\Apis\Reversementbonus@show']);
Route::GET('api/eden/transaction/reversementbonus/search/{key}/{value}', ['as' => 'api/eden/transaction/reversementbonus/search/{key}/{value}', 'uses' => 'Eden\Transaction\Apis\Reversementbonus@search']);
Route::POST('api/eden/transaction/reversementbonus', ['as' => 'api/eden/transaction/reversementbonus', 'uses' => 'Eden\Transaction\Apis\Reversementbonus@store']);
Route::PUT('api/eden/transaction/reversementbonus/{id}', ['as' => 'api/eden/transaction/reversementbonus', 'uses' => 'Eden\Transaction\Apis\Reversementbonus@update']);
Route::DELETE('api/eden/transaction/reversementbonus/{id}', ['as' => 'api/eden/transaction/reversementbonus', 'uses' => 'Eden\Transaction\Apis\Reversementbonus@delete']);

Route::GET('api/eden/transaction/transferts/all', ['as' => 'api/eden/transaction/transferts/all', 'uses' => 'Eden\Transaction\Apis\Transferts@index']);
Route::GET('api/eden/transaction/transferts/find/{id}', ['as' => 'api/eden/transaction/transferts/find/{id}', 'uses' => 'Eden\Transaction\Apis\Transferts@show']);
Route::GET('api/eden/transaction/transferts/search/{key}/{value}', ['as' => 'api/eden/transaction/transferts/search/{key}/{value}', 'uses' => 'Eden\Transaction\Apis\Transferts@search']);
Route::POST('api/eden/transaction/transferts', ['as' => 'api/eden/transaction/transferts', 'uses' => 'Eden\Transaction\Apis\Transferts@store']);
Route::PUT('api/eden/transaction/transferts/{id}', ['as' => 'api/eden/transaction/transferts', 'uses' => 'Eden\Transaction\Apis\Transferts@update']);
Route::DELETE('api/eden/transaction/transferts/{id}', ['as' => 'api/eden/transaction/transferts', 'uses' => 'Eden\Transaction\Apis\Transferts@delete']);

