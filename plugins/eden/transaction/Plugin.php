<?php namespace Eden\Transaction;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
	
	 public function registerComponents()
    {
        return [
        
			'Eden\Transaction\Components\FrontendCrudAchatunites'=>'FrontendCrudAchatunites',
			'Eden\Transaction\Components\ListeAchatunites'=>'ListeAchatunites',
			'Eden\Transaction\Components\FrontendCrudFraisinscriptions'=>'FrontendCrudFraisinscriptions',
			'Eden\Transaction\Components\ListeFraisinscriptions'=>'ListeFraisinscriptions',
			'Eden\Transaction\Components\FrontendCrudRechargementcomptes'=>'FrontendCrudRechargementcomptes',
			'Eden\Transaction\Components\ListeRechargementcomptes'=>'ListeRechargementcomptes',
			'Eden\Transaction\Components\FrontendCrudRetraitargents'=>'FrontendCrudRetraitargents',
			'Eden\Transaction\Components\ListeRetraitargents'=>'ListeRetraitargents',
			'Eden\Transaction\Components\FrontendCrudReversementbonus'=>'FrontendCrudReversementbonus',
			'Eden\Transaction\Components\ListeReversementbonus'=>'ListeReversementbonus',
			'Eden\Transaction\Components\FrontendCrudTransferts'=>'FrontendCrudTransferts',
			'Eden\Transaction\Components\ListeTransferts'=>'ListeTransferts',
			'Eden\Transaction\Components\RetraitArgent'=>'RetraitArgent',
			'Eden\Transaction\Components\TransfertArgent'=>'TransfertArgent',
			'Eden\Transaction\Components\ListeRetraitargents'=>'ListeRetraits',
			'Eden\Transaction\Components\ListeTransactions'=>'ListeTransactions',

        ];
    }
    
    public function registerSettings()
    {
    }
    
}