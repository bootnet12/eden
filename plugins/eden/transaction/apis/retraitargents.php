<?php namespace Eden\Transaction\Apis;
    
use Cms\Classes\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;

class Retraitargents extends Controller
{
    public function index(){

        $data = \Eden\Transaction\Models\Retraitargent::all()->toArray();

         return [
                "code" => 200,
                "data" => $data
            ];
    }

    public function show($id){

        $data = \Eden\Transaction\Models\Retraitargent::where('id',$id)->get();

        if($data){

           return [
                "code" => 200,
                "data" => $data
            ];
        }
        return [
                   "code" => 400,
                   "message" => "bad request"
               ];
    }
    
    public function search($key,$value){

        $data = \Eden\Transaction\Models\Retraitargent::where($key,$value)->get();

        if($data){

           return [
                "code" => 200,
                "data" => $data
            ];
        }
        return [
                   "code" => 400,
                   "message" => "bad request"
               ];
    }

    public function store(Request $request){

    	try {
            $arr = $request->all();
            $instance = new \Eden\Transaction\Models\Retraitargent() ;    

            while ( $data = current($arr)) {
                $instance->{key($arr)} = $data;
                next($arr);
            }
            $instance->save();
            return [
                    "code" => 200,
                    "data" => $instance
                   ];
        }
        catch (Exception $ex) {
            $ex->getMessage();
            return $ex;
        }
    }

    public function update($id, Request $request){

        $status = \Eden\Transaction\Models\Retraitargent::where('id',$id)->update($request->all());
    
        if($status){
            return [
                    "code" => 200,
                    "data" => "Data has been updated successfully."
                   ];
        }else{
            return [
                    "code" => 400,
                    "data" => "Error, data failed to update."
                   ];
        }
    }

    public function delete($id){

        \Eden\Transaction\Models\Retraitargent::where('id',$id)->delete();
        return [
                   "code" => 200,
                   "data" => "Data has been deleted successfully."
                ];
    }

     
}