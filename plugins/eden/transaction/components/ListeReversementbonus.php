<?php namespace  Eden\Transaction\Components;

use Cms\Classes\Page;
use Auth;
use DB;

class ListeReversementbonus extends \Cms\Classes\ComponentBase
{    
    public function componentDetails(){
        return [
            'name' => 'ListeReversementbonus',
            'description' => 'ListeReversementbonus'
        ];
    }
	
	public function defineProperties() {
        return [
		
		'itemsPerPage' => [
				'title'             => 'Nombre d\'items par page',
				'type'              => 'string',
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'Entrer un entier',
				'default'           => 12,
			],
		'viewPage' => [
                'title'             => 'View page',
                'type'              => 'dropdown',
                'group'             => 'Links',
                'options'           => $this->getPageOptions()
            ],
		'updatePage' => [
                'title'             => 'Update page',
                'type'              => 'dropdown',
                'group'             => 'Links',
                'options'           => $this->getPageOptions()
            ],
            
            
        ];
    }

	public function onRun(){ 
        $user = Auth::getUser();
        // dd($user->id);
        $this->page['records'] = \Eden\Transaction\Models\Reversementbonus::where('adherent_id', $user->id)
        ->orderBy('created_at', 'desc')->paginate($this->property('itemsPerPage'),$this->param('page'));
		$this->page['viewpage']=$this->property('viewPage');
		$this->page['updatepage']=$this->property('updatePage');
    }
	
	public function getPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }
	
	
	
} 