<?php namespace  Eden\Transaction\Components;
use Eden\Transaction\Models\Fraisinscription as Record ;
use Redirect;
use Cms\Classes\Page;

use RainLab\User\Models\User as User;
use Eden\Configuration\Models\Statustransaction as Statustransaction;
class FrontendCrudFraisinscriptions extends \Cms\Classes\ComponentBase
{    
    public $mode;
	public $id;
	
    public function componentDetails(){
        return [
            'name' => 'FrontendCrudFraisinscriptions',
            'description' => 'FrontendCrudFraisinscriptions'
        ];
    }
	
	public function defineProperties() {
         return [
			'mode' => [
				'title'       => 'Mode',
				'type'        => 'dropdown',
				'default'     => 'create',
				'description' => 'Selectionez le mode ',
				'options'     => ['create'=>'Create', 'update'=>'Update','view'=>'View']
			],
			'redirectPage' => [
					'title'             => 'Redirect page',
					'type'              => 'dropdown',
					'group'             => 'Links',
					'options'           => $this->getRedirectOptions()
				],
			'pageParameter' => [
					'title'             => 'Redirect page parameter',
					'type'              => 'dropdown',
					'default'           => 'Nothing',
					'group'             => 'Links',
					'options'     => ['id'=>'Id', 'nothing'=>'Nothing']
				],
			
        ];
    }

	public function onRun(){ 
	    $this->page['id']=$this->id=$this->param('id');
        $this->page['mode']=$this->mode=$this->property('mode');
		$this->prepareVars();
		if ($this->mode != 'create' and $this->id){
		$this->page['record']= Record::find($this->id);
		 
		}
    }
	
	public function onCreate(){
        $data = post();		
	    $this->page['mode']=$this->mode=$this->property('mode');
		$record= new Record();
		if ($this->mode == 'create'){
		$record->fill($data);
		$record->save();
		if ($this->property('pageParameter')!='nothing'){
		       return Redirect::to($this->property('redirectPage')."/".$record->id);
		  
		   }
		   
		   else{
		       return Redirect::to($this->property('redirectPage'));
		  
		   }
		}
		
    }
	
	public function onUpdate(){
		$data = post();
        $this->page['id']=$this->id=$this->param('id');		
	    $this->page['mode']=$this->mode=$this->property('mode');
		if ($this->mode == 'update' and $this->id){
		$record= Record::find($this->id);
		$record->fill($data);
		$record->save();
		if ($this->property('pageParameter')!='nothing'){
		       return Redirect::to($this->property('redirectPage')."/".$record->id);
		  
		   }
		   
		   else{
		       return Redirect::to($this->property('redirectPage'));
		  
		   }
		}
		
    }
	public function onView(){ 
	    $this->page['id']=$this->param('id');
        $this->page['mode']=$this->mode=$this->property('mode');
		if ($this->mode == 'update' and $this->id){
		$record= Record::find($this->id);
		$record= Record::find($this->id);
		}
		
    }
	
	public function prepareVars(){ 
	    
		$this->page['adherent_ids']=User::all();
		$this->page['valideur_ids']=User::all();
		$this->page['status_ids']=Statustransaction::all();
		
    }
	
	public function getRedirectOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }
	
	
	
} 