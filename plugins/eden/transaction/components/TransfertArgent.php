<?php namespace  Eden\Transaction\Components;

use Cms\Classes\Page;
use Eden\Transaction\Models\Transfert;
use Redirect;
use Flash;
use Auth;
use RainLab\User\Models\User;
use ValidationException;
use DB;
use Lang;

class TransfertArgent extends \Cms\Classes\ComponentBase
{    
    public function componentDetails(){
        return [
            'name' => 'TransfertArgent',
            'description' => 'TransfertArgent'
        ];
    }
	
	public function defineProperties() {
        return [
		
		'itemsPerPage' => [
				'title'             => 'Nombre d\'items par page',
				'type'              => 'string',
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'Entrer un entier',
				'default'           => 12,
			],
		'viewPage' => [
                'title'             => 'View page',
                'type'              => 'dropdown',
                'group'             => 'Links',
                'options'           => $this->getPageOptions()
            ],
		'updatePage' => [
                'title'             => 'Update page',
                'type'              => 'dropdown',
                'group'             => 'Links',
                'options'           => $this->getPageOptions()
            ],
            
            
        ];
    }

	public function onRun(){ 
        $this->page['montant'] = get("montant");
        $this->page['destinateur'] = User::find(get('destinataire_id'));
        // Recuperation des adherants
        $this->page['adherants'] = User::where('id', '<>', Auth::getUser()->id)->get();
		/*$this->page['records'] = \Eden\Transaction\Models\Retraitargent::paginate($this->property('itemsPerPage'),$this->param('page'));;
		$this->page['viewpage']=$this->property('viewPage');
		$this->page['updatepage']=$this->property('updatePage');*/
        $user = $this->page['user'] = Auth::getUser();
        $this->page['errorMessageNoValidate'] = Lang::get('eden.gestion::lang.app.message_user_no_validate');
    }

    public function onRequestTransfertArgent(){
        // recuperation du user
        $user = Auth::getUser();
        $data = \Input::all();
        // On vérifie le solde
        if($user->solde < $data['montant']){
            throw new ValidationException(["error" => "Le montant saisi doit être inférieur au solde !"]);
        }
        // Recuperation de l'adheran
        $userTransfert = User::where('code_parrainage', $data['code_parrainage'])->first();
        if(!$userTransfert){
            throw new ValidationException(["error" => "L'utilisateur n'existe pas !"]);
        }
        return \Redirect::to("transfert-confirmation?montant=".$data['montant']."&destinataire_id=".$userTransfert->id);
    }
	
	public function getPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onConfirmTransfertArgent(){
        DB::beginTransaction();
        try{
            $expediteur = Auth::getUser();
            $data = post();

            // On vérifie le solde
            if($expediteur->solde < $data['montant']){
                throw new ValidationException(["error" => "Le montant saisi est supérieur au solde !"]);
            }
            
            $retrait  = new Transfert;
            $retrait->montant = $data['montant'];
            $retrait->expediteur_id = $expediteur->id;
            $retrait->destinataire_id = $data['destinataire_id'];
            $retrait->save();

            // Transfert d'argent 
            $expediteur->solde = $expediteur->solde - $data['montant'];
            $expediteur->save();
            $destinataire = User::find($data['destinataire_id']);
            $destinataire->solde = $destinataire->solde + $data['montant'];
            $destinataire->save();

            DB::commit();
            return Redirect::to("/transfert-success");
        }catch(\Exception $e){
            trace_log("Une erreur est survenue lors du transfert d'argent ".$e->getMessage());
            \Flash::error("Une erreur est survenue lors du transfert d'argent !");
            DB::rollBack();
        }
    }
	
	
	
} 