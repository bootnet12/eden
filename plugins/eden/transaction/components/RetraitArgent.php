<?php namespace  Eden\Transaction\Components;

use Cms\Classes\Page;
use Eden\Transaction\Models\Retraitargent as RetraitArgentModel;
use Eden\Configuration\Models\ModePaiement as ModePaiementModel;
use Redirect;
use Flash;
use Auth;
use ValidationException;
use Lang;

class RetraitArgent extends \Cms\Classes\ComponentBase
{    
    public function componentDetails(){
        return [
            'name' => 'ListeTransferts',
            'description' => 'ListeTransferts'
        ];
    }
	
	public function defineProperties() {
        return [
		
		'itemsPerPage' => [
				'title'             => 'Nombre d\'items par page',
				'type'              => 'string',
				'validationPattern' => '^[0-9]+$',
				'validationMessage' => 'Entrer un entier',
				'default'           => 12,
			],
		'viewPage' => [
                'title'             => 'View page',
                'type'              => 'dropdown',
                'group'             => 'Links',
                'options'           => $this->getPageOptions()
            ],
		'updatePage' => [
                'title'             => 'Update page',
                'type'              => 'dropdown',
                'group'             => 'Links',
                'options'           => $this->getPageOptions()
            ],
            
            
        ];
    }

	public function onRun(){ 
        $this->page['montant'] = get("montant");
        $this->page['telephone'] = get("telephone");
        $modepaiement = ModePaiementModel::find(get("mode_paiement"));
        if($modepaiement)
            $this->page['mode_paiement'] = $modepaiement;
        $this->page['modepaiements'] = ModePaiementModel::all();
		/*$this->page['records']= \Eden\Transaction\Models\Retraitargent::paginate($this->property('itemsPerPage'),$this->param('page'));;
		$this->page['viewpage']=$this->property('viewPage');
		$this->page['updatepage']=$this->property('updatePage');*/
        $this->page['errorMessageNoValidate'] = Lang::get('eden.gestion::lang.app.message_user_no_validate');
    }

    public function onRequestRetraitArgent(){
        $data = \Input::all();
        $user = Auth::getUser();
        // On vérifie le solde
        if($user->solde < $data['montant']){
            throw new ValidationException(["error" => "Le montant saisi doit être inférieur au solde !"]);
        }
        // Le montant de retrait doit être supérieur à 10.0000
        if($data['montant'] < 10000){
            throw new ValidationException(["error" => "Le montant de retrait doit être supérieur à 10.000 FCFA !"]);
        }
        // On vérifie que le tééphone est saisis
        if(!$data['telephone']){
            throw new ValidationException(["error" => "Le téléphone est requis !"]);
        }
        // On vérifie le mode de paiement
        if(!$data['mode_paiement']){
            throw new ValidationException(["error" => "Le mode de paiement est requis !"]);
        }
        return \Redirect::to("retrait-confirmation?montant=".$data['montant']."&mode_paiement=".$data['mode_paiement']."&telephone=".$data['telephone']);
    }
	
	public function getPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRetraitArgent(){
        $user = Auth::getUser();
        $data = post();
        $retrait  = new RetraitargentModel;
        $retrait->montant = $data['montant'];
        $retrait->adherent_id = $user->id;
        $retrait->telephone = $data['telephone'];
        $retrait->mode_paiement = $data['mode_paiement'];
        $retrait->status_id = 1;
        $retrait->save();
        return Redirect::to("/retrait-success");
    }
	
	
	
} 