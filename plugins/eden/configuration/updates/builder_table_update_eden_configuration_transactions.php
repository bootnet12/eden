<?php namespace Eden\Configuration\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdenConfigurationTransactions extends Migration
{
    public function up()
    {
        Schema::table('eden_configuration_transactions', function($table)
        {
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::table('eden_configuration_transactions', function($table)
        {
            $table->dropColumn('user_id');
        });
    }
}
