<?php namespace Eden\Configuration\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdenConfigurationTemoingnage extends Migration
{
    public function up()
    {
        Schema::create('eden_configuration_temoingnage', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('temoignant');
            $table->text('description');
            $table->string('fontion_temoignant');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('eden_configuration_temoingnage');
    }
}
