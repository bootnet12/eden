<?php namespace Eden\Configuration\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdenConfigurationTyutilisateur extends Migration
{
    public function up()
    {
        Schema::create('eden_configuration_tyutilisateur', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('libelle', 50);
            $table->text('description');
            $table->integer('firstuser')->nullable()->unsigned();
            $table->integer('lastuser')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('eden_configuration_tyutilisateur');
    }
}
