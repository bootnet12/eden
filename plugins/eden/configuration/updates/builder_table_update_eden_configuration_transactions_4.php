<?php namespace Eden\Configuration\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdenConfigurationTransactions4 extends Migration
{
    public function up()
    {
        Schema::table('eden_configuration_transactions', function($table)
        {
            $table->dropColumn('type_transaction');
        });
    }
    
    public function down()
    {
        Schema::table('eden_configuration_transactions', function($table)
        {
            $table->text('type_transaction');
        });
    }
}
