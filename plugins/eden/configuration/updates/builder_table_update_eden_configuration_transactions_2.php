<?php namespace Eden\Configuration\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdenConfigurationTransactions2 extends Migration
{
    public function up()
    {
        Schema::table('eden_configuration_transactions', function($table)
        {
            $table->renameColumn('user_id', 'adherant_id');
        });
    }
    
    public function down()
    {
        Schema::table('eden_configuration_transactions', function($table)
        {
            $table->renameColumn('adherant_id', 'user_id');
        });
    }
}
