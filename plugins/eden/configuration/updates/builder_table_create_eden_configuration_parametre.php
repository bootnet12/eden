<?php namespace Eden\Configuration\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateEdenConfigurationParametre extends Migration
{
    public function up()
    {
        Schema::create('eden_configuration_parametre', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('cle', 100);
            $table->string('valeur', 100);
            $table->text('description');
            $table->integer('firstuser')->nullable()->unsigned();
            $table->integer('lastuser')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('eden_configuration_parametre');
    }
}
