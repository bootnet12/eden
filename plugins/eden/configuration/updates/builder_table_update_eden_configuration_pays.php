<?php namespace Eden\Configuration\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdenConfigurationPays extends Migration
{
    public function up()
    {
        Schema::rename('eden_configuration_tyutilisateur', 'eden_configuration_pays');
        Schema::table('eden_configuration_pays', function($table)
        {
            $table->renameColumn('libelle', 'nom');
        });
    }
    
    public function down()
    {
        Schema::rename('eden_configuration_pays', 'eden_configuration_tyutilisateur');
        Schema::table('eden_configuration_tyutilisateur', function($table)
        {
            $table->renameColumn('nom', 'libelle');
        });
    }
}
