<?php namespace Eden\Configuration\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEdenConfigurationTransactions3 extends Migration
{
    public function up()
    {
        Schema::table('eden_configuration_transactions', function($table)
        {
            $table->renameColumn('adherant_id', 'adherent_id');
        });
    }
    
    public function down()
    {
        Schema::table('eden_configuration_transactions', function($table)
        {
            $table->renameColumn('adherent_id', 'adherant_id');
        });
    }
}
