<?php namespace Eden\Configuration;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
	
	 public function registerComponents()
    {
        return [
        
			'Eden\Configuration\Components\FrontendCrudDevisebonus'=>'FrontendCrudDevisebonus',
			'Eden\Configuration\Components\ListeDevisebonus'=>'ListeDevisebonus',
			'Eden\Configuration\Components\FrontendCrudOperateurs'=>'FrontendCrudOperateurs',
			'Eden\Configuration\Components\ListeOperateurs'=>'ListeOperateurs',
			'Eden\Configuration\Components\FrontendCrudParametres'=>'FrontendCrudParametres',
			'Eden\Configuration\Components\ListeParametres'=>'ListeParametres',
			'Eden\Configuration\Components\FrontendCrudPays'=>'FrontendCrudPays',
			'Eden\Configuration\Components\ListePays'=>'ListePays',
			'Eden\Configuration\Components\FrontendCrudStatusachatunites'=>'FrontendCrudStatusachatunites',
			'Eden\Configuration\Components\ListeStatusachatunites'=>'ListeStatusachatunites',
			'Eden\Configuration\Components\FrontendCrudStatusadherents'=>'FrontendCrudStatusadherents',
			'Eden\Configuration\Components\ListeStatusadherents'=>'ListeStatusadherents',
			'Eden\Configuration\Components\FrontendCrudStatustransactions'=>'FrontendCrudStatustransactions',
			'Eden\Configuration\Components\ListeStatustransactions'=>'ListeStatustransactions',
			'Eden\Configuration\Components\FrontendCrudTypeproduits'=>'FrontendCrudTypeproduits',
			'Eden\Configuration\Components\ListeTypeproduits'=>'ListeTypeproduits',
			'Eden\Configuration\Components\Contact'=>'Contact',

        ];
    }
    
    public function registerSettings()
    {
    }
    
}