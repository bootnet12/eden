<?php namespace Eden\Configuration\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Banniere extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
	public $formConfig = 'config_form.yaml';
	// public $relationConfig = 'config_relation.yaml';
	//public $reorderConfig = 'config_reorder.yaml';
	// public $importExportConfig = 'config_import_export.yaml';
	//public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Eden.Configuration', 'configuration', 'banniere');
    }
}
