<?php namespace Eden\Configuration\Controllers;

class Index extends \Backend\Classes\Controller{

    public $requiredPermissions = ['eden.configuration'];
    public function __construct(){
        parent::__construct();
        \BackendMenu::setContext('Eden.Configuration','configuration');
    }

    public function index(){}

}