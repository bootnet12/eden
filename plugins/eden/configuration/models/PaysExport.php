<?php namespace Eden\Configuration\Models;

class PaysExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Eden\Configuration\Models\Pays::all();
        
            $records->each(function($record) use ($columns) {
                
    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}