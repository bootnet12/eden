<?php namespace Eden\Configuration\Models;

use Mail;
use BackendAuth;
class Statustransaction extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_configuration_statustransaction';

	protected $fillable = ['id','libelle','description','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"libelle" => "Libellé",
		"description" => "Description",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
	];

	public $hasMany = [
        'fraisinscriptions' => [
            'Eden\Transaction\Models\Fraisinscription',
            'key' => 'status_id',
        ],
        'rechargementcomptes' => [
            'Eden\Transaction\Models\Rechargementcompte',
            'key' => 'status_id',
        ],
        'retraitargents' => [
            'Eden\Transaction\Models\Retraitargent',
            'key' => 'status_id',
        ],
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}