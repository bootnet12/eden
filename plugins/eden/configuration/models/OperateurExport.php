<?php namespace Eden\Configuration\Models;

class OperateurExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        
        $records = \Eden\Configuration\Models\Operateur::all();
        
            $records->each(function($record) use ($columns) {
                
			 if ($record->pays) $record->r_pays = $record->pays->code;

    
                    
            $record->addVisible($columns);
                
            });

            return  $records->toArray();
    }
}