<?php namespace Eden\Configuration\Models;

use Mail;
use BackendAuth;
class Operateur extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_configuration_operateur';

	protected $fillable = ['id','nom','pays','description','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"nom" => "Nom",
		"pays" => "Pays",
		"description" => "Description",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
		'pays'=>[\Eden\Configuration\Models\Pays::class],
	];

	public $hasMany = [
        'achatunites' => [
            'Eden\Transaction\Models\Achatunites',
            'key' => 'operateur_id',
        ],
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}