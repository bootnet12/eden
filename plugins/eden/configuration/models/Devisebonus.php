<?php namespace Eden\Configuration\Models;

use Mail;
use BackendAuth;
class Devisebonus extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_configuration_devisebonus';

	protected $fillable = ['id','symbole','nom','description','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"symbole" => "Symbole",
		"nom" => "Nom",
		"description" => "Description",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
	];

	public $hasMany = [
        'produits' => [
            'Eden\Gestion\Models\Produit',
            'key' => 'devisebonus_id',
        ],
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}