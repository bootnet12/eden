<?php namespace Eden\Configuration\Models;

use Mail;
use BackendAuth;
class Pays extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_configuration_pays';

	protected $fillable = ['id','code','nom','description','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"code" => "Code",
		"nom" => "Nom",
		"description" => "Description",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
	];

	public $hasMany = [
        'operateurs' => [
            'Eden\Configuration\Models\Operateur',
            'key' => 'pays_id',
        ],
        'comptebancaires' => [
            'Eden\Gestion\Models\Comptebancaire',
            'key' => 'pays_id',
        ],
        'comptemobiles' => [
            'Eden\Gestion\Models\Comptemobile',
            'key' => 'pays_id',
        ],
        'achatunites' => [
            'Eden\Transaction\Models\Achatunites',
            'key' => 'pays_id',
        ],
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}