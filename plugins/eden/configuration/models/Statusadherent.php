<?php namespace Eden\Configuration\Models;

use Mail;
use BackendAuth;
class Statusadherent extends\Jojo\Models\JojoModel {

	//use \October\Rain\Database\Traits\Validation;
	//use \October\Rain\Database\Traits\Nullable;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	public $table = 'eden_configuration_statusadherent';

	protected $fillable = ['id','libelle','description','firstuser','lastuser','created_at','updated_at','deleted_at',];

	public $attributeNames = [
		"id" => "Id",
		"libelle" => "Libellé",
		"description" => "Description",
		"firstuser" => "Firstuser",
		"lastuser" => "Lastuser",
		"created_at" => "Created_at",
		"updated_at" => "Updated_at",
		"deleted_at" => "Deleted_at",
	];

	public $belongsTo = [
	];

	public $hasMany = [
	];

	public $belongsToMany = [
	];

	public $attachOne = [
	];

	public $attachMany = [
	];

}