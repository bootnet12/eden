<?php namespace Eden\Configuration\Models;

use Model;

/**
 * Model
 */
class Temoignage extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'eden_configuration_temoingnage';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
