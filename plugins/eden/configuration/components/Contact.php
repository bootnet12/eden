<?php namespace  Eden\Configuration\Components;

use Cms\Classes\Page;
use Eden\Configuration\Models\Parametre;


class Contact extends \Cms\Classes\ComponentBase
{    
    public function componentDetails(){
        return [
            'name' => 'Contact',
            'description' => 'Contact'
        ];
    }
	
	public function onRun(){ 
		$this->page['situationGeographique']= Parametre::where('cle', 'situation-geographique')->first();//  '\Eden\Configuration\Models\Pays::paginate($this->property('itemsPerPage'),$this->param('page'));;
        $this->page['email']= Parametre::where('cle', 'email')->first();
        $this->page['tel1']= Parametre::where('cle', 'tel1')->first();
        $this->page['tel2']= Parametre::where('cle', 'tel2')->first();
        $this->page['tel3']= Parametre::where('cle', 'tel3')->first();
        $this->page['localisation']= Parametre::where('cle', 'localisation')->first();
		$this->page['viewpage']=$this->property('viewPage');
		$this->page['updatepage']=$this->property('updatePage');
    }
} 