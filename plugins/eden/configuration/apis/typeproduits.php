<?php namespace Eden\Configuration\Apis;
    
use Cms\Classes\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;

class Typeproduits extends Controller
{
    public function index(){

        $data = \Eden\Configuration\Models\Typeproduit::all()->toArray();

         return [
                "code" => 200,
                "data" => $data
            ];
    }

    public function show($id){

        $data = \Eden\Configuration\Models\Typeproduit::where('id',$id)->get();

        if($data){

           return [
                "code" => 200,
                "data" => $data
            ];
        }
        return [
                   "code" => 400,
                   "message" => "bad request"
               ];
    }
    
    public function search($key,$value){

        $data = \Eden\Configuration\Models\Typeproduit::where($key,$value)->get();

        if($data){

           return [
                "code" => 200,
                "data" => $data
            ];
        }
        return [
                   "code" => 400,
                   "message" => "bad request"
               ];
    }

    public function store(Request $request){

    	try {
            $arr = $request->all();
            $instance = new \Eden\Configuration\Models\Typeproduit() ;    

            while ( $data = current($arr)) {
                $instance->{key($arr)} = $data;
                next($arr);
            }
            $instance->save();
            return [
                    "code" => 200,
                    "data" => $instance
                   ];
        }
        catch (Exception $ex) {
            $ex->getMessage();
            return $ex;
        }
    }

    public function update($id, Request $request){

        $status = \Eden\Configuration\Models\Typeproduit::where('id',$id)->update($request->all());
    
        if($status){
            return [
                    "code" => 200,
                    "data" => "Data has been updated successfully."
                   ];
        }else{
            return [
                    "code" => 400,
                    "data" => "Error, data failed to update."
                   ];
        }
    }

    public function delete($id){

        \Eden\Configuration\Models\Typeproduit::where('id',$id)->delete();
        return [
                   "code" => 200,
                   "data" => "Data has been deleted successfully."
                ];
    }

     
}