<?php 

Route::GET('api/eden/configuration/devisebonus/all', ['as' => 'api/eden/configuration/devisebonus/all', 'uses' => 'Eden\Configuration\Apis\Devisebonus@index']);
Route::GET('api/eden/configuration/devisebonus/find/{id}', ['as' => 'api/eden/configuration/devisebonus/find/{id}', 'uses' => 'Eden\Configuration\Apis\Devisebonus@show']);
Route::GET('api/eden/configuration/devisebonus/search/{key}/{value}', ['as' => 'api/eden/configuration/devisebonus/search/{key}/{value}', 'uses' => 'Eden\Configuration\Apis\Devisebonus@search']);
Route::POST('api/eden/configuration/devisebonus', ['as' => 'api/eden/configuration/devisebonus', 'uses' => 'Eden\Configuration\Apis\Devisebonus@store']);
Route::PUT('api/eden/configuration/devisebonus/{id}', ['as' => 'api/eden/configuration/devisebonus', 'uses' => 'Eden\Configuration\Apis\Devisebonus@update']);
Route::DELETE('api/eden/configuration/devisebonus/{id}', ['as' => 'api/eden/configuration/devisebonus', 'uses' => 'Eden\Configuration\Apis\Devisebonus@delete']);

Route::GET('api/eden/configuration/operateurs/all', ['as' => 'api/eden/configuration/operateurs/all', 'uses' => 'Eden\Configuration\Apis\Operateurs@index']);
Route::GET('api/eden/configuration/operateurs/find/{id}', ['as' => 'api/eden/configuration/operateurs/find/{id}', 'uses' => 'Eden\Configuration\Apis\Operateurs@show']);
Route::GET('api/eden/configuration/operateurs/search/{key}/{value}', ['as' => 'api/eden/configuration/operateurs/search/{key}/{value}', 'uses' => 'Eden\Configuration\Apis\Operateurs@search']);
Route::POST('api/eden/configuration/operateurs', ['as' => 'api/eden/configuration/operateurs', 'uses' => 'Eden\Configuration\Apis\Operateurs@store']);
Route::PUT('api/eden/configuration/operateurs/{id}', ['as' => 'api/eden/configuration/operateurs', 'uses' => 'Eden\Configuration\Apis\Operateurs@update']);
Route::DELETE('api/eden/configuration/operateurs/{id}', ['as' => 'api/eden/configuration/operateurs', 'uses' => 'Eden\Configuration\Apis\Operateurs@delete']);

Route::GET('api/eden/configuration/parametres/all', ['as' => 'api/eden/configuration/parametres/all', 'uses' => 'Eden\Configuration\Apis\Parametres@index']);
Route::GET('api/eden/configuration/parametres/find/{id}', ['as' => 'api/eden/configuration/parametres/find/{id}', 'uses' => 'Eden\Configuration\Apis\Parametres@show']);
Route::GET('api/eden/configuration/parametres/search/{key}/{value}', ['as' => 'api/eden/configuration/parametres/search/{key}/{value}', 'uses' => 'Eden\Configuration\Apis\Parametres@search']);
Route::POST('api/eden/configuration/parametres', ['as' => 'api/eden/configuration/parametres', 'uses' => 'Eden\Configuration\Apis\Parametres@store']);
Route::PUT('api/eden/configuration/parametres/{id}', ['as' => 'api/eden/configuration/parametres', 'uses' => 'Eden\Configuration\Apis\Parametres@update']);
Route::DELETE('api/eden/configuration/parametres/{id}', ['as' => 'api/eden/configuration/parametres', 'uses' => 'Eden\Configuration\Apis\Parametres@delete']);

Route::GET('api/eden/configuration/pays/all', ['as' => 'api/eden/configuration/pays/all', 'uses' => 'Eden\Configuration\Apis\Pays@index']);
Route::GET('api/eden/configuration/pays/find/{id}', ['as' => 'api/eden/configuration/pays/find/{id}', 'uses' => 'Eden\Configuration\Apis\Pays@show']);
Route::GET('api/eden/configuration/pays/search/{key}/{value}', ['as' => 'api/eden/configuration/pays/search/{key}/{value}', 'uses' => 'Eden\Configuration\Apis\Pays@search']);
Route::POST('api/eden/configuration/pays', ['as' => 'api/eden/configuration/pays', 'uses' => 'Eden\Configuration\Apis\Pays@store']);
Route::PUT('api/eden/configuration/pays/{id}', ['as' => 'api/eden/configuration/pays', 'uses' => 'Eden\Configuration\Apis\Pays@update']);
Route::DELETE('api/eden/configuration/pays/{id}', ['as' => 'api/eden/configuration/pays', 'uses' => 'Eden\Configuration\Apis\Pays@delete']);

Route::GET('api/eden/configuration/statusachatunites/all', ['as' => 'api/eden/configuration/statusachatunites/all', 'uses' => 'Eden\Configuration\Apis\Statusachatunites@index']);
Route::GET('api/eden/configuration/statusachatunites/find/{id}', ['as' => 'api/eden/configuration/statusachatunites/find/{id}', 'uses' => 'Eden\Configuration\Apis\Statusachatunites@show']);
Route::GET('api/eden/configuration/statusachatunites/search/{key}/{value}', ['as' => 'api/eden/configuration/statusachatunites/search/{key}/{value}', 'uses' => 'Eden\Configuration\Apis\Statusachatunites@search']);
Route::POST('api/eden/configuration/statusachatunites', ['as' => 'api/eden/configuration/statusachatunites', 'uses' => 'Eden\Configuration\Apis\Statusachatunites@store']);
Route::PUT('api/eden/configuration/statusachatunites/{id}', ['as' => 'api/eden/configuration/statusachatunites', 'uses' => 'Eden\Configuration\Apis\Statusachatunites@update']);
Route::DELETE('api/eden/configuration/statusachatunites/{id}', ['as' => 'api/eden/configuration/statusachatunites', 'uses' => 'Eden\Configuration\Apis\Statusachatunites@delete']);

Route::GET('api/eden/configuration/statusadherents/all', ['as' => 'api/eden/configuration/statusadherents/all', 'uses' => 'Eden\Configuration\Apis\Statusadherents@index']);
Route::GET('api/eden/configuration/statusadherents/find/{id}', ['as' => 'api/eden/configuration/statusadherents/find/{id}', 'uses' => 'Eden\Configuration\Apis\Statusadherents@show']);
Route::GET('api/eden/configuration/statusadherents/search/{key}/{value}', ['as' => 'api/eden/configuration/statusadherents/search/{key}/{value}', 'uses' => 'Eden\Configuration\Apis\Statusadherents@search']);
Route::POST('api/eden/configuration/statusadherents', ['as' => 'api/eden/configuration/statusadherents', 'uses' => 'Eden\Configuration\Apis\Statusadherents@store']);
Route::PUT('api/eden/configuration/statusadherents/{id}', ['as' => 'api/eden/configuration/statusadherents', 'uses' => 'Eden\Configuration\Apis\Statusadherents@update']);
Route::DELETE('api/eden/configuration/statusadherents/{id}', ['as' => 'api/eden/configuration/statusadherents', 'uses' => 'Eden\Configuration\Apis\Statusadherents@delete']);

Route::GET('api/eden/configuration/statustransactions/all', ['as' => 'api/eden/configuration/statustransactions/all', 'uses' => 'Eden\Configuration\Apis\Statustransactions@index']);
Route::GET('api/eden/configuration/statustransactions/find/{id}', ['as' => 'api/eden/configuration/statustransactions/find/{id}', 'uses' => 'Eden\Configuration\Apis\Statustransactions@show']);
Route::GET('api/eden/configuration/statustransactions/search/{key}/{value}', ['as' => 'api/eden/configuration/statustransactions/search/{key}/{value}', 'uses' => 'Eden\Configuration\Apis\Statustransactions@search']);
Route::POST('api/eden/configuration/statustransactions', ['as' => 'api/eden/configuration/statustransactions', 'uses' => 'Eden\Configuration\Apis\Statustransactions@store']);
Route::PUT('api/eden/configuration/statustransactions/{id}', ['as' => 'api/eden/configuration/statustransactions', 'uses' => 'Eden\Configuration\Apis\Statustransactions@update']);
Route::DELETE('api/eden/configuration/statustransactions/{id}', ['as' => 'api/eden/configuration/statustransactions', 'uses' => 'Eden\Configuration\Apis\Statustransactions@delete']);

Route::GET('api/eden/configuration/typeproduits/all', ['as' => 'api/eden/configuration/typeproduits/all', 'uses' => 'Eden\Configuration\Apis\Typeproduits@index']);
Route::GET('api/eden/configuration/typeproduits/find/{id}', ['as' => 'api/eden/configuration/typeproduits/find/{id}', 'uses' => 'Eden\Configuration\Apis\Typeproduits@show']);
Route::GET('api/eden/configuration/typeproduits/search/{key}/{value}', ['as' => 'api/eden/configuration/typeproduits/search/{key}/{value}', 'uses' => 'Eden\Configuration\Apis\Typeproduits@search']);
Route::POST('api/eden/configuration/typeproduits', ['as' => 'api/eden/configuration/typeproduits', 'uses' => 'Eden\Configuration\Apis\Typeproduits@store']);
Route::PUT('api/eden/configuration/typeproduits/{id}', ['as' => 'api/eden/configuration/typeproduits', 'uses' => 'Eden\Configuration\Apis\Typeproduits@update']);
Route::DELETE('api/eden/configuration/typeproduits/{id}', ['as' => 'api/eden/configuration/typeproduits', 'uses' => 'Eden\Configuration\Apis\Typeproduits@delete']);

